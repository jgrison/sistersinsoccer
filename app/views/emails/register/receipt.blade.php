<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			body { font-family: sans-serif; }
			.details { width: 100%; font-family: sans-serif; font-size: 14px; border-right: 1px solid #eee; border-left: 1px solid #eee; border-top: 1px solid #eee; }
			.details td { border-bottom: 1px solid #eee;}
			.details tr:nth-child(even) { background-color: #f8fafc; }
			.details tr:nth-child(odd) { background-color: #fafbfc; }
		</style>
	</head>
	<body>

		<table width="100%" bgcolor="f2f5f8" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center">
					<table width="650" cellpadding="40" bgcolor="ffffff" cellspacing="0" style="border: 1px solid #e9ecef;">
						<tr>
							<td>
								<table width="100%" style="border-bottom: 1px solid #eee;">
									<tr>
										<td>
											<center>
												<img src="{{ asset('img/logo.jpg') }}" width="250">
											</center>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%">
									<tr>
										<td>
											<center>
												<h2 style="margin: 0;">THANKS FOR REGISTERING</h2>
												<p>Your registration and payment of <strong>${{ (float)$session->price + ((float)$session->price * 0.13) }}</strong> (HST Included) has been recieved!</p>
												<p>Please check <a href="http://sistasinsoccer.com/">www.sistasinsoccer.com</a> <br/>for further updates and information on scheduling.</p>
											</center>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			<tr/>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
