<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
		<style>
			.details { width: 100%; font-family: sans-serif; font-size: 14px; border-right: 1px solid #eee; border-left: 1px solid #eee; border-top: 1px solid #eee; }
			.details td { border-bottom: 1px solid #eee;}
			.details tr:nth-child(even) { background-color: #f8fafc; }
			.details tr:nth-child(odd) { background-color: #fafbfc; }
		</style>
	</head>
	<body>

		<table width="100%" bgcolor="ffffff" cellpadding="0" cellspacing="0">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align="center">
					<table width="650" cellpadding="40" bgcolor="ffffff" cellspacing="0" borderstyle="border: 1px solid #e9ecef;">
						<tr>
							<td>
								<table width="100%">
									<tr>
										<td>
											<center>
												<img src="{{ asset('img/logo.jpg') }}" width="250">
											</center>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
								</table>
								<!-- details --->
								<table class="details" cellpadding="20" cellspacing="0">
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Name</td>
										<td width="400" style="color: #748490;">{{ $user->first_name }} {{ $user->last_name }}</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Email</td>
										<td width="400" style="color: #748490;">{{ $user->email }}</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Phone</td>
										<td width="400" style="color: #748490;">{{ $user->phone }}</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Address</td>
										<td width="400" style="color: #748490;">{{ $user->address }}, {{ $user->city }} {{ $user->province }}, {{ $user->postal_code }}</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Session</td>
										<td width="400" style="color: #748490;">{{ $session->location->name }} / {{ $session->name }}</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Position</td>
										<td width="400" style="color: #748490;">{{ $registration->position }}</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Goalie?</td>
										<td width="400" style="color: #748490;">@if($registration->goalie == "1") Yes @else No @endif</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Captain?</td>
										<td width="400" style="color: #748490;">@if($registration->captain == "1") Yes @else No @endif</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Code</td>
										<td width="400" style="color: #748490;">{{ $registration->code }}</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Paid</td>
										<td width="400" style="color: #748490;">${{ (float)$registration->price() + ((float)$registration->price() * 0.13) }}</td>
									</tr>
									<tr>
										<td width="200" style="border-right: 1px solid #eee; text-align: center; font-weight: bold; color: #4a5055;">Paid On</td>
										<td width="400" style="color: #748490;">{{ $registration->created_at }}</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>
	</body>
</html>
