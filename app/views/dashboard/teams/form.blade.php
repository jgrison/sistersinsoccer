@extends('master.dashboard')

@section('content')
<div id="teams">
	@if(isset($team)) {{ Form::open(array('url' => 'dashboard/teams/edit_post', 'files' => true)) }} @else {{ Form::open(array('url' => 'dashboard/teams/create_post', 'files' => true)) }} @endif
		<input name="id" type="hidden" value="@if(isset($team)){{ $team->id }}@endif">
		<select name="location">
			<option value="">Select A Location</options>
			@foreach($locations as $location)
				<option value="{{ $location->id }}" @if(isset($team) && $team->location_id == $location->id) selected="selected" @endif>{{ $location->name }}</option>
			@endforeach
		</select>
		<input name="name" placeholder="Team Name" value="@if(isset($team)){{ $team->name }}@endif">
		<button>Submit</button>
	{{ Form::close() }}
</div>
@stop
