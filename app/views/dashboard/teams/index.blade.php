@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
		Manage Teams
		</div>
		<div class="button">
			<div class="btn" data-toggle="modal" data-target="#create-team">
				Add Team
			</div>
		</div>
	</div>
</div>
<div class="page">
	<div id="teams">
		<ul class="list">
			<li class="title">
				<div class="session">Session</div>
				<div class="name">Name</div>
			</li>
		@foreach($teams as $team)
			<li data-id="{{ $team->id }}" data-session="{{ $team->session_id }}" data-name="{{ $team->name }}">
				<div class="session">{{ $team->session->name }}</div>
				<div class="name">{{ $team->name }}</div>
				<a class="edit" href="#" data-toggle="modal" data-target="#edit-team">Edit</a>
				<span>|</span>
				<a class="delete" href="{{ URL::to('dashboard/teams/delete/'.$team->id) }}">Delete</a>
			</li>
		@endforeach
		</ul>
	</div>
</div>

<div class="modal fade" id="create-team" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
				<div class="modal-body">
					{{ Form::open(array('url' => 'dashboard/teams/create_post', 'files' => true)) }}
					<div class="form">
						<select name="session">
							<option value="">Select A Session</options>
							@foreach($sessions as $session)
								<option value="{{ $session->id }}">{{ $session->name }}</option>
							@endforeach
						</select>
						<input name="name" placeholder="Team Name">
					</div>
					<button>
						<img src="{{ asset('img/icons/check.png') }}">
						<p>Add Team</p>
					</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit-team" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
				<div class="modal-body">
					{{ Form::open(array('url' => 'dashboard/teams/edit_post', 'files' => true)) }}
					<div class="form">
						<input name="id" type="hidden" value="">
						<select name="session">
							<option value="">Select A Session</options>
							@foreach($sessions as $session)
								<option value="{{ $session->id }}">{{ $session->name }}</option>
							@endforeach
						</select>
						<input name="name" placeholder="Field Name" value="">
					</div>
					<button>
						<img src="{{ asset('img/icons/check.png') }}">
						<p>Edit Team</p>
					</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#teams .list li .edit').click(function() {
			// Reset Session
			$('#edit-team input').each(function() {
				$(this).val();
			});
			$('select[name="sessions"]').val();

			var team = $(this).parent();

			$('input[name="id"]').val(team.data('id'));
			$('select[name="session"]').val(team.data('session'));
			$('input[name="name"]').val(team.data('name'));
		});

		$('#teams .list li .delete').click(function() {
			return confirm("Are you sure you want to delete?");
		});
	});
</script>
@stop
