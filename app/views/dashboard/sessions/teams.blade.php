@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
			{{ $team->name }}
		</div>
		<div class="button">
		</div>
	</div>
</div>
<div class="page">
	<div id="sessions">
		<ul class="list">
			<li class="title">
				<div class="name">Name</div>
				<div class="email">Email</div>
				<div class="phone">Phone</div>
				<div class="cell">Cell</div>
				<div class="captain">Captain</div>
				<div class="goalie">Goalie</div>
			</li>
		@foreach($team->getTeamPlayers($session->id) as $player)
			<li data-session-id="{{ $session->id }}" data-team-id="{{ $team->id }}" data-user-id="{{ $player->player()->id }}">
				<div class="name">{{ $player->player()->first_name }} {{ $player->player()->last_name }}</div>
				<div class="email">{{ $player->player()->email }}</div>
				<div class="phone">@if($player->player()->phone) {{ $player->player()->phone }} @else - @endif</div>
				<div class="cell">@if($player->player()->cell) {{ $player->player()->cell }} @else - @endif</div>
				<div class="captain">
					<div class="check  @if($player->captain) checked @endif"></div>
				</div>
				<div class="goalie">
					<div class="check  @if($player->goalie) checked @endif"></div>
				</div>
			</li>
		@endforeach
		</ul>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#sessions .list li .captain').click(function() {
			$.post( "{{ URL::to('dashboard/teams/captain/assign') }}", { session : $(this).parent().data('session-id'), team : $(this).parent().data('team-id'), user : $(this).parent().data('user-id') }, function( data ) {
				location.reload();
			});
		});
		$('#sessions .list li .goalie').click(function() {
			$.post( "{{ URL::to('dashboard/teams/goalie/assign') }}", { session : $(this).parent().data('session-id'), team : $(this).parent().data('team-id'), user : $(this).parent().data('user-id') }, function( data ) {
				location.reload();
			});
		});
	});
</script>
@stop
