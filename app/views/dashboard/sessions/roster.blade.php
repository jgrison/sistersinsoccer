@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
			{{ $session->name }} | {{ $session->location->name }}
		</div>
		<div class="button">
		</div>
	</div>
</div>
<div class="page">
	<div id="sessions">
		<ul class="teams">
		@foreach($session->teams as $team)
			<li>
				<div class="name">{{ $team->name }}</div>
				<div class="players">
					<h1>{{ count($team->getTeamPlayers($session->id)) }}</h1>
					<h5>Players</h5>
				</div>
				<a href="{{ URL::to('dashboard/sessions/teams/'.$team->id.'?id='.$session->id) }}">
					<div class="button">View Roster</div>
				</a>
			</li>
		@endforeach
		</ul>
	</div>
</div>
@stop
