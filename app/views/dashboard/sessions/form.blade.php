@extends('master.dashboard')

@section('content')
<div id="sessions">
	@if(isset($session)) {{ Form::open(array('url' => 'dashboard/sessions/edit_post', 'files' => true)) }} @else {{ Form::open(array('url' => 'dashboard/sessions/create_post', 'files' => true)) }} @endif
		<input name="id" type="hidden" value="@if(isset($session)){{ $session->id }}@endif">
		<select name="location">
			<option value="">Select A Location</options>
			@foreach($locations as $location)
				<option value="{{ $location->id }}" @if(isset($session) && $session->location_id == $location->id) selected="selected" @endif>{{ $location->name }}</option>
			@endforeach
		</select>
		<input name="name" placeholder="Session Name" value="@if(isset($session)){{ $session->name }}@endif">
		<input name="price" placeholder="Session Price" value="@if(isset($session)){{ $session->price }}@endif">
		<input name="start_date" placeholder="Session Start Date" value="@if(isset($session)){{ $session->start_date }}@endif">
		<input name="end_date" placeholder="Session End Date" value="@if(isset($session)){{ $session->end_date }}@endif">

		<button>Submit</button>
	{{ Form::close() }}
</div>
@stop
