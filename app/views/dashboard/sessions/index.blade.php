@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
			Sessions
		</div>
		<div class="button">
			<div class="btn" data-toggle="modal" data-target="#create-session">
				Create Session
			</div>
		</div>
	</div>
</div>
<div class="page">
	<div id="sessions">
		<ul class="list">
			<li class="title">
				<div class="registration">Registration</div>
				<div class="name">Name</div>
				<div class="date">Start Date</div>
			</li>
		@foreach($sessions as $session)
			<li data-id="{{ $session->id }}" data-location="{{ $session->location_id }}" data-name="{{ $session->name }}" data-price="{{ $session->price }}" data-start="{{ $session->start_date }}" data-end="{{ $session->end_date }}">
				<div class="registration">
					@if($session->active == 1)
						<div class="status active">Active</div>
					@else
						<div class="status closed">Closed</div>
					@endif
				</div>
				<div class="name">{{ $session->name }}</div>
				<div class="date">{{ date("F j, Y", strtotime($session->start_date)) }}</div>
				<a href="{{ URL::to('dashboard/sessions/view/'.$session->id) }}">Registrations ({{ count($session->registrations) }})</a>
				<a href="{{ URL::to('dashboard/sessions/roster/'.$session->id) }}">Roster</a>
				<span> | </span>
				<a href="#" class="edit" data-toggle="modal" data-target="#edit-session">Edit</a>
				<a class="disable" href="{{ URL::to('dashboard/sessions/status/'.$session->id) }}">Close</a>
				<a class="delete" href="{{ URL::to('dashboard/sessions/delete/'.$session->id) }}">Delete</a>
			</li>
		@endforeach
		</ul>
	</div>
</div>

<div class="modal fade" id="create-session" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
				<div class="modal-body">
					{{ Form::open(array('url' => 'dashboard/sessions/create_post', 'files' => true)) }}
					<div class="form">
						<select name="location">
							<option value="">Select A Location</options>
							@foreach($locations as $location)
								<option value="{{ $location->id }}">{{ $location->name }}</option>
							@endforeach
						</select>
						<input name="name" placeholder="Session Name">
						<input name="price" placeholder="Session Price">
						<input name="start_date" class="datepicker" placeholder="Session Start Date">
						<input name="end_date" class="datepicker" placeholder="Session End Date">
					</div>
					<button>
						<img src="{{ asset('img/icons/check.png') }}">
						<p>Create Session</p>
					</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit-session" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
				<div class="modal-body">
					{{ Form::open(array('url' => 'dashboard/sessions/edit_post', 'files' => true)) }}
					<div class="form">
						<input name="id" type="hidden" value="">
						<select name="location">
							<option value="">Select A Location</options>
							@foreach($locations as $location)
								<option value="{{ $location->id }}">{{ $location->name }}</option>
							@endforeach
						</select>
						<input name="name" placeholder="Session Name" value="">
						<input name="price" placeholder="Session Price" value="">
						<input name="start_date" class="datepicker" placeholder="Session Start Date" value="">
						<input name="end_date" class="datepicker" placeholder="Session End Date" value="">
					</div>
					<button>
						<img src="{{ asset('img/icons/check.png') }}">
						<p>Edit Session</p>
					</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#sessions .list li .edit').click(function() {
			// Reset Session
			$('#edit-session input').each(function() {
				$(this).val();
			});
			$('select[name="location"]').val();

			var session = $(this).parent();

			$('input[name="id"]').val(session.data('id'));
			$('select[name="location"]').val(session.data('location'));
			$('input[name="name"]').val(session.data('name'));
			$('input[name="price"]').val(session.data('price'));
			$('input[name="start_date"]').val(session.data('start'));
			$('input[name="end_date"]').val(session.data('end'));
		});

		$('#sessions .list li .delete').click(function() {
			return confirm("Are you sure you want to delete?");
		});

		$('#sessions .list li .disable').click(function() {
			return confirm("Are you sure you want to close this session?");
		});
	});
</script>
@stop
