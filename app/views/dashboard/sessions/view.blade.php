@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
			{{ $session->name }} | {{ $session->location->name }}
		</div>
		<div class="button">
		</div>
	</div>
</div>
<div class="page">
	<div id="sessions">
		<ul class="list registration">
			<li class="title">
				<div class="name" data-name="name">Name</div>
				<div class="goalie" data-name="goalie">Goalie</div>
				<div class="captain" data-name="captain">Captain</div>
				<div class="code" data-name="code">Code</div>
				<div class="paid" data-name="paid">Paid</div>
				<div class="assign">Assign</div>
			</li>
		@foreach($session->registrations as $registration)
			@if($registration->user)
			<li @if($registration->getTeam()) class="assigned" @else class="unassigned" @endif data-name="{{ $registration->user->first_name }} {{ $registration->user->last_name }}" data-goalie="@if($registration->goalie == 1) Yes @else No @endif" data-captain="@if($registration->captian == 1) Yes @else No @endif" data-code="{{ $registration->code }}">
				<div class="name">{{ $registration->user->first_name }} {{ $registration->user->last_name }}</div>
				<div class="goalie">@if($registration->goalie == 1) Yes @else No @endif</div>
				<div class="captain">@if($registration->captian == 1) Yes @else No @endif</div>
				<div class="code">@if($registration->code){{ $registration->code }}@else - @endif</div>
				<div class="paid">
					Yes
				</div>
				<div class="assign">
					<select name="teams" data-session-id="{{ $session->id }}" data-user-id="{{ $registration->user->id }}">
						<option value="unassign">Unassigned</option>
						@foreach($session->teams as $team)
							<option value="{{ $team->id }}" @if($registration->getTeam() && $registration->getTeam()->team_id == $team->id) selected="selected" @endif>{{ $team->name }}</option>
						@endforeach
					</select>
				</div>
				<a href="{{ URL::to('dashboard/registration/delete/'.$registration->id) }}">
					<div class="delete">
						x
					</div>
				</a>
			</li>
			@endif
		@endforeach
		</ul>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#sessions .list li').each(function() {
			if($(this).hasClass('unassigned'))
			{
				$(this).insertAfter("#sessions .list li:nth-child(1)");
			}
		});

		$('select[name="teams"]').change(function() {
			$.post( "{{ URL::to('dashboard/sessions/team/enroll') }}", { session : $(this).data('session-id'), team : $(this).val(), user : $(this).data('user-id') }, function( data ) {
				location.reload();
			});
		});

		$("#sessions .title div").click(function() {
				$("#sessions .list li").sort(sorting).appendTo('#sessions .list');
				window.id = $(this).data('name');
				function sorting(a, b) {
					return ($(b).data(window.id)) < ($(a).data(window.id)) ? 1 : -1;
				}
		});

		$('#sessions .list li .delete').click(function() {
			return confirm("Are you sure you want to delete?");
		});
	});
</script>
@stop
