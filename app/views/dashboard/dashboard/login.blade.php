@extends('master.login')

@section('content')
<div id="login">
	<img src="{{ asset('img/logo.jpg') }}">
	{{ Form::open(array('url' => 'login')) }}

	{{ Form::text('email', Input::old('email'), array('placeholder' => 'Enter Your Email')) }}
	{{ Form::password('password', array('placeholder' => 'Enter Your Password')) }}
	<button>Sign In</button>
	{{ Form::close() }}
</div>
@stop
