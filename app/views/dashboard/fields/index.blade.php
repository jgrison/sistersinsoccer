@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
		Manage Fields
		</div>
		<div class="button">
			<div class="btn" data-toggle="modal" data-target="#create-field">
				Add Field
			</div>
		</div>
	</div>
</div>
<div class="page">
	<div id="fields">
		<ul class="list">
			<li class="title">
				<div class="location">Location</div>
				<div class="name">Name</div>
				<div class="address">Address</div>
			</li>
		@foreach($fields as $field)
			<li data-id="{{ $field->id }}" data-location="{{ $field->location->id }}" data-name="{{ $field->name }}" data-address="{{ $field->address }}">
				<div class="location">{{ $field->location->name }}</div>
				<div class="name">{{ $field->name }}</div>
				<div class="address">{{ $field->address }}</div>
				<a class="edit" href="#" data-toggle="modal" data-target="#edit-field">Edit</a>
				<span>|</span>
				<a href="{{ URL::to('dashboard/field/delete/'.$field->id) }}" class="delete">Delete</a>
			</li>
		@endforeach
		</ul>
	</div>
</div>

<div class="modal fade" id="create-field" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
				<div class="modal-body">
					{{ Form::open(array('url' => 'dashboard/field/create_post', 'files' => true)) }}
					<div class="form">
						<select name="location">
							<option value="">Select A Location</options>
							@foreach($locations as $location)
								<option value="{{ $location->id }}">{{ $location->name }}</option>
							@endforeach
						</select>
						<input name="name" placeholder="Field Name">
						<input name="address" placeholder="Field Address">
					</div>
					<button>
						<img src="{{ asset('img/icons/check.png') }}">
						<p>Add Field</p>
					</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="edit-field" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
				<div class="modal-body">
					{{ Form::open(array('url' => 'dashboard/field/edit_post', 'files' => true)) }}
					<div class="form">
						<input name="id" type="hidden" value="">
						<select name="location">
							<option value="">Select A Location</options>
							@foreach($locations as $location)
								<option value="{{ $location->id }}">{{ $location->name }}</option>
							@endforeach
						</select>
						<input name="name" placeholder="Field Name" value="">
						<input name="address" placeholder="Field Address" value="">
					</div>
					<button>
						<img src="{{ asset('img/icons/check.png') }}">
						<p>Edit Field</p>
					</button>
					{{ Form::close() }}
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#fields .list li .edit').click(function() {
			// Reset Session
			$('#edit-field input').each(function() {
				$(this).val();
			});
			$('select[name="location"]').val();

			var field = $(this).parent();

			$('input[name="id"]').val(field.data('id'));
			$('select[name="location"]').val(field.data('location'));
			$('input[name="name"]').val(field.data('name'));
			$('input[name="address"]').val(field.data('address'));
		});

		$('#fields .list li .delete').click(function() {
			return confirm("Are you sure you want to delete?");
		});
	});
</script>
@stop
