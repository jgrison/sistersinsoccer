@extends('master.dashboard')

@section('content')
<div id="sessions">
	@if(isset($field)) {{ Form::open(array('url' => 'dashboard/field/edit_post', 'files' => true)) }} @else {{ Form::open(array('url' => 'dashboard/field/create_post', 'files' => true)) }} @endif
		<input name="id" type="hidden" value="@if(isset($field)){{ $field->id }}@endif">
		<select name="location">
			<option value="">Select A Location</options>
			@foreach($locations as $location)
				<option value="{{ $location->id }}" @if(isset($field) && $field->location_id == $location->id) selected="selected" @endif>{{ $location->name }}</option>
			@endforeach
		</select>
		<input name="name" placeholder="Field Name" value="@if(isset($field)){{ $field->name }}@endif">
		<input name="address" placeholder="Field Address" value="@if(isset($field)){{ $field->address }}@endif">

		<button>Submit</button>
	{{ Form::close() }}
</div>
@stop
