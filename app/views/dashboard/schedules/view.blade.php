@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
			Schedule
		</div>
		<div class="button">
			weee
		</div>
	</div>
</div>
<div class="page">
	<div id="schedule">
		<?php $count = 1; ?>
		@foreach($schedule as $day)
			<div class="title">
				<h3>{{ date("l F j, Y", strtotime($day->date)) }}</h3>
				<p>Game #{{ $count }}</p>
			</div>
			<ul class="list">
				<li class="category">
					<div class="home">Home</div>
					<div class="vs">&nbsp;</div>
					<div class="away">Away</div>
					<div class="field">Field</div>
					<div class="time">Time</div>
				</li>
				@foreach($day->games as $game)
					<li>
						<div class="home">@if($game->getHomeTeam()){{ $game->getHomeTeam()->name }}@endif</div>
						<div class="vs">vs.</div>
						<div class="away">@if($game->getAwayTeam()){{ $game->getAwayTeam()->name }}@endif</div>
						<div class="field">{{ $game->field->name }}</div>
						<div class="time">{{ $day->time }}</div>
					</li>
				@endforeach
			</ul>
			<?php $count++; ?>
		@endforeach
	</div>
</div>
@stop
