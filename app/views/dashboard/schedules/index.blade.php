@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
			Game Scheduling
		</div>
		<div class="button">

		</div>
	</div>
</div>
<div class="page">
	<div id="schedule">
		<ul class="list">
			<li class="title">
				<div class="name">Name</div>
				<div class="location">Location</div>
				<div class="teams">Teams</div>
			</li>
		@foreach($sessions as $session)
			<li>
				<div class="name">{{ $session->name }}</div>
				<div class="location">{{ $session->location->name }}</div>
				<div class="teams">-</div>
				<div class="menu">
					@if(!$session->hasSchedule())
					<a href="{{ URL::to('dashboard/schedule/create?id='.$session->id) }}">Generate Schedule</a>
					@endif
					<span>|</span>
					<a href="{{ URL::to('dashboard/schedule/view/'.$session->id) }}">View</a>
					<span>|</span>
					<a href="{{ URL::to('dashboard/schedules') }}">Export</a>
				</div>
			</li>
		@endforeach
		</ul>
	</div>
</div>
@stop
