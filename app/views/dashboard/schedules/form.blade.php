@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
			Generate Schedule
		</div>
		<div class="button">

		</div>
	</div>
</div>
<div class="page">
	<div id="schedule">
		{{ Form::open(array('url' => 'dashboard/schedule/create_post', 'files' => true)) }}
			<div class="titling">
				<h4>Select Fields</h4>
			</div>
			<ul class="fields">
				@foreach($session->location->fields as $field)
					<li data-id="{{ $field->id }}">
						<div class="check"></div>
						<h5>{{ $field->name }}</h5>
					</li>
				@endforeach
			</ul>
			<div class="titling">
				<h4>Select Teams</h4>
			</div>
			<ul class="teams">
				@foreach($teams as $team)
					<li data-id="{{ $team->id }}">
						<div class="check"></div>
						<h5>{{ $team->name }}</h5>
					</li>
				@endforeach
			</ul>
			<div class="titling">
				<h4>Other Options</h4>
			</div>
			<div class="options">
				<input name="total_games" placeholder="Total Games">
				<select name="days">
					<option>Select a Day</option>
					<option value="Monday">Monday</option>
					<option value="Tuesday">Tuesday</option>
					<option value="Wednesday">Wednesday</option>
					<option value="Thursday">Thursday</option>
					<option value="Friday">Friday</option>
					<option value="Saturday">Saturday</option>
					<option value="Sunday">Sunday</option>
				</select>
				<input class="clear datepicker" name="start_date" placeholder="Start Date">
				<input name="end_date" placeholder="End Date" class="datepicker">
				<input name="time" placeholder="Time">

				<input name="id" type="hidden" value="@if(isset($session)){{ $session->id }}@endif">
				<input name="fields" type="hidden" value="">
				<input name="teams" type="hidden" value="">
			</div>
			<div class="generate">
				<button>Generate Schedule</button>
			</div>
		{{ Form::close() }}
	</div>
</div>

<script>
	$(document).ready(function() {
		$('input[name="fields"], input[name="team"]').val();

		$('.fields li').click(function() {
			if($(this).hasClass('checked'))
			{
				$(this).removeClass('checked');
				$('input[name="fields"]').val($('input[name="fields"]').val().replace($(this).data('id')+",", ''));
			}
			else
			{
				$('input[name="fields"]').val($('input[name="fields"]').val() + $(this).data('id') + ",");
				$(this).addClass('checked');
			}
		});

		$('.teams li').click(function() {
			if($(this).hasClass('checked'))
			{
				$(this).removeClass('checked');
				$('input[name="teams"]').val($('input[name="teams"]').val().replace($(this).data('id')+",", ''));
			}
			else
			{
				$('input[name="teams"]').val($('input[name="teams"]').val() + $(this).data('id') + ",");
				$(this).addClass('checked');
			}
		});
	});
</script>
@stop
