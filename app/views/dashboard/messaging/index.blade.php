@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
		Messaging Centre
		</div>
		<div class="button">
			<a href="{{ URL::to('dashboard/messaging/create') }}">
				<div class="btn">Send Message</div>
			</a>
		</div>
	</div>
</div>
<div class="page">
	<div id="messaging">
		<ul class="list">
			@foreach($messages as $message)
				<div class="subject">{{ $message->subject }}</div>
				<div class="sent">{{ $message->created_at }}</div>
				<div class="attachment">{{ asset($message->attachment) }}</div>
			@endforeach
		</ul>
	</div>
</div>
@stop
