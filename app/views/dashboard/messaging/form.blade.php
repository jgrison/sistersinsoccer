@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
		Send a Message
		</div>
		<div class="button">
		</div>
	</div>
</div>
<div class="page">
	<div id="messages">
		<div class="overview">
			<h5>SISTAS IN SOCCER</h5>
			<h1>Messaging Tips</h1>
			<div class="divide"></div>
			<ul>
				<li>Select <strong>All Leagues</strong> to send an email to everyone.</li>
				<li>Select a <strong>League</strong> to send an email to all teams in that league.</li>
				<li>Select a <strong>Team</strong> to only send an email to that team.</li>
			</ul>
		</div>
		<div class="form">
			{{ Form::open(array('url' => 'dashboard/messaging/create_post', 'files' => true)) }}
				<select name="session">
					<option value="">Select a League</option>
					<option value="all">All Leagues</option>
					@foreach($sessions as $session)
						<option value="{{ $session->id }}">{{ $session->name }}</option>
					@endforeach
				</select>
				<select name="team" class="clear">
					<option value="">Select a Team</option>
					@foreach($sessions as $session)
						@foreach($session->teams as $team)
						<option value="{{ $team->id }}">{{ $team->name }}</option>
						@endforeach
					@endforeach
				</select>
				<input class="subject" name="subject" placeholder="Enter a Subject">
				<textarea name="message" placeholder="Type Your Message Here"></textarea>
				<div class="file">
					{{ Form::file('attachment') }}
				</div>
				<button>Send Message</button>
			{{ Form::close() }}
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#teams .list li .delete').click(function() {
			return confirm("Are you sure you want to delete?");
		});
	});
</script>
@stop
