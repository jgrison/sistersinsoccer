@extends('master.master')

@section('content')
<div id="register">
	<div class="thankyou">
		<h1>THANK YOU FOR SIGNING UP!</h1>
		<p>A copy of your payment receipt has been emailed to you. </br>If you do not receive an email, please contact us. Please do not register again.</p>
		<a href="http://www.sistasinsoccer.com">
			<div class="button">Return to Sistas In Soccer Website</div>
		</a>
	</div>
</div>
@stop
