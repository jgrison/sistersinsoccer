@extends('master.master')

@section('content')

	<div id="register">
		<div class="form">
			{{ Form::open(array('url' => 'register/create_post', 'files' => true)) }}
			<div class="title">
				<h1>Personal Details</h1>
			</div>
			<div class="group">
				<h5>First Name</h5>
				<input name="first_name" placeholder="Enter a First Name">
			</div>
			<div class="group">
				<h5>Last Name</h5>
				<input name="last_name" placeholder="Enter a Last Name">
			</div>
			<div class="group clear">
				<h5>DOB</h5>
				<input name="dob" placeholder="Date of Birth (MM/DD/YYYY)">
			</div>
			<div class="group">
				<h5>Email</h5>
				<input name="email" placeholder="Enter an Email">
			</div>
			<div class="group">
				<h5>Password</h5>
				<input type="password" name="password" placeholder="Enter a Password">
			</div>
			<div class="group clear">
				<h5>Confirm Password</h5>
				<input type="password" name="confirm_password" placeholder="Confirm Password">
			</div>
			<div class="group">
				<h5>Phone Number</h5>
				<input name="phone" placeholder="Enter a Phone Number">
			</div>
			<div class="group">
				<h5>Cell Number (Optional)</h5>
				<input name="cell" placeholder="Enter a Cell Number">
			</div>
			<div class="group clear">
				<h5>Address</h5>
				<input name="address" placeholder="Address">
			</div>
			<div class="group ">
				<h5>City</h5>
				<input name="city" placeholder="City">
			</div>

			<div class="group">
				<h5>Province</h5>
				<select name="province">
					<option value="">Province</option>
					<option value="AB">AB</option>
					<option value="BC">BC</option>
					<option value="MB">MB</option>
					<option value="NB">NB</option>
					<option value="NL">NL</option>
					<option value="NS">NS</option>
					<option value="NT">NT</option>
					<option value="NU">NU</option>
					<option value="ON">ON</option>
					<option value="PE">PE</option>
					<option value="QC">QC</option>
					<option value="SK">SK</option>
					<option value="YT">YT</option>
				</select>
			</div>
			<div class="group clear">
				<h5>Postal Code</h5>
				<input name="postal_code" placeholder="Postal Code">
			</div>
			<br style="clear: both;"/>
			<div class="title">
				<h1>Position</h1>
			</div>
			<div class="group">
				<h5>Position</h5>
				<input name="position" placeholder="Enter Position">
			</div>
			<div class="group half space">
				<h5>Goalie</h5>
				<select name="goalie">
					<option selected="selected">Goalie?</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</div>
			<div class="group half">
				<h5>Captian</h5>
				<select name="captain">
					<option selected="selected">Captain?</option>
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
			</div>
			<div class="group clear">
				<h5>Code</h5>
				<input name="code" placeholder="Enter a Code / Player Request">
			</div>
			<div class="title">
				<h1>Select a League</h1>
			</div>
			<ul class="locations">
				@foreach($locations as $location)
					@foreach($location->sessions as $session)
						<li data-id="{{ $session->id }}">
							<div class="check"></div>
							<div class="location">{{ $session->name }}</div>
							<div class="price-total"><strong>Total:</strong> ${{ (float)$session->price + ((float)$session->price * 0.13) }} </div>
							<div class="price">${{ $session->price }} + HST</div>
						</li>
					@endforeach
				@endforeach
			</ul>
			<input type="hidden" name="session" value="1">
			<button>Register</button>
			{{ Form::close() }}
		</div>
	</div>

	<script>
		$(document).ready(function() {
			$('#register .locations li').click(function() {
				var location = $(this);

				if(location.hasClass('selected'))
				{
					location.removeClass('selected');
					location.find('.check').removeClass('checked');
					$('input[name="session"]').val('');
				}
				else
				{
					// Clear
					$('input[name="session"]').val('');
					$('#register .locations li').each(function() {
						$(this).removeClass('selected');
						$(this).find('.check').removeClass('checked');
					});

					location.addClass('selected');
					location.find('.check').addClass('checked');
					$('input[name="session"]').val(location.data('id'));
				}
			});
		});
	</script>
@stop
