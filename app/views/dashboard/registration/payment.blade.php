@extends('master.master')

@section('content')
<div id="register">
	<div class="details">
		<div class="text">
			<h5>{{ $session->location->name }}</h5>
			<h4>{{ $session->name }} - {{ $session->field_name }}</h4>
		</div>
		<div class="price">
			<h5>TOTAL</h5>
			<h1>${{ (float)$session->price + ((float)$session->price * 0.13) }}</h1>
		</div>
	</div>
	<div class="checkout">
	{{ Form::open(array('url' => 'register/payments/create_post', 'files' => true)) }}
		<input type="hidden" name="id" value="{{ $registration->id }}">
		<input type="hidden" name="amount" value="{{ (float)$session->price + ((float)$session->price * 0.13) }}">
		<div class="card">
			<img src="{{ asset('img/icons/cards.png') }}">
			<input name="card" placeholder="Credit Card Number">
		</div>
		<div class="cvc">
			<input name="cvc" placeholder="CVC">
		</div>
		<div class="date">
			<select name="exp_month">
				<option value="">Expiry Month</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
			</select>
		</div>
		<div class="date">
			<select name="exp_year">
				<option value="">Expiry Year</option>
				@for($i = 2016; $i <= 2025; $i++)
				<option value="{{ $i }}">{{ $i }}</option>
				@endfor
			</select>
		</div>
		<br style="clear:both;"/>
		<div class="cards">
			<img src="{{ asset('img/icons/visa.png') }}">
			<img src="{{ asset('img/icons/mastercard.png') }}">
			<img src="{{ asset('img/icons/amex.png') }}">
		</div>
		<button>Pay Fees</button>
	{{ Form::close() }}
	</div>
	<div class="disclaimer">
		For your safety and privacy, we don't store any credit card information on our servers. All payments are processed securely by Stripe.<br/>
		Need to pay another way?

		@if($session->location)
			@if($session->location->name == "London")
				<a href="http://www.sistasinsoccer.com/london.php#contact">Contact Us</a>
			@endif
			@if($session->location->name == "Hamilton")
				<a href="http://www.sistasinsoccer.com/hamilton.php#contact">Contact Us</a>
			@endif
			@if($session->location->name == "Guelph")
				<a href="http://www.sistasinsoccer.com/guelph.php#contact">Contact Us</a>
			@endif
			@if($session->location->name == "Windsor")
				<a href="http://www.sistasinsoccer.com/windsor.php#contact">Contact Us</a>
			@endif
		@endif
	</div>
</div>
@stop
