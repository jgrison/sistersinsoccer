@extends('master.master')

@section('content')
	<div id="register">
		<div class="waiver">
			{{ Form::open(array('url' => 'register/waiver/create_post', 'files' => true)) }}
				<p>Recognizing the risks presented by the sport of soccer, the signature below indicates a knowing, voluntary release of any claim which might be asserted against Sistas in Sports Inc., the city of London, the city of Hamilton, the city of Windsor and the city of Guelph, its officers, administrative assistants, coaches, assistant coaches, managers, volunteers and any other agents representing its officers, agents or representatives and Western Counties Soccer Association of which these clubs are a member. By waiving any right to assert a claim, I am agreeing to release, absolve, indemnify and hold harmless any and all parties previously mentioned for any and all liability rising from any injuries incurred by participant in relationship to the sponsored and arranged activities to the Sistas in Sports Inc., the city of London, the city of Hamilton, the city of Windsor, the city of Guelph and Western Counties Soccer Association. My waiver means that I as a participant, accept and assume all risks and hazards inherent in and related to the activities of the Sistas in Sports Inc., the city of London, the city of Hamilton, the city of Windsor, the city of Guelph and Western Counties Soccer Association.</p>
				<!-- @if($sessions->location->id == 1)
					<h4>NEW THIS SEASON</h4>
					<p>Both  London Sunday and Friday will be 60 minute games!  Last season we tried having 90-minute games for the Sunday league.  While some people liked the change, the overwhelming response was to stick to one-hour games.</p>

					<h4>SEASON START</h4>
					<p>The season will start in early May.  The year-end tournament will be on the weekend of September 9 to 11, 2016.</p>
					<p>We will not schedule any games on the long weekends, so there will be no games on Victoria Day weekend, Canada Day weekend, Civic Holiday weekend and Labour Day weekend. The regular season will run to the end of August.  </p>
					<p>Once the schedule is made, you will be notified by email and can download it from the website.  We have a new scheduler program this season that will make things so much easier!</p>
					<p>For the London leagues, the season will begin with round robin play for 7 games. After the first 7 games, all leagues will be divided into two or three pools, (dependent on league size).  The regular season will then begin and the games will “count” towards the play offs after the pools have been divided.</p>

					<h4>FIELDS</h4>
					<p>There are maps to the fields available on our website.</p>

					<h4>SISSY OF THE YEAR</h4>
					<p>Our goal is fun first, so we like to highlight the importance of this with the Sissy of the Game nomination. </p>
					<p>For those of you who are new to us this season, this is an award held in high esteem. For every game played, each team will nominate one player from their team to be the Sissy. This individual will have best exemplified a positive attitude and encouraging spirit during the game. This is not necessarily the most skilled player on the team. At the end of the season, the nominations are tallied and one player from each team wins an award. She who has the most fun wins!</p>

					<h4>CAPTAINS AND COACHES</h4>
					<p>There will be a captain’s and coaches meeting held at Stoney Creek Baptist Church on Tuesday April 19, 2016 at 7 pm.  Captains will be notified by email. </p>
					<p>It is imperative that the captains (and coaches if your team has one) from each team be at this meeting or sends a representative. This is when a lot of critical information is given out, as well as all the team equipment, including balls, net, clipboard, jerseys etc. </p>
					<h4>MISCELLANEOUS BITS</h4>

					<p>By early April, you will be receiving your team roster by email.  This has the name and phone number of each person on your team. If you haven’t received this information by two weeks prior to your season opener, please contact us.</p>
					<p>For insurance purposes, only registered Sistas in Soccer players can play in the league.  All players must be on your team roster, not from another Sistas team.  Our insurance coverage requires that each player have soccer cleats (not runners) and shin guards for every game. It is imperative that you have your equipment for each game or you will not be allowed to play.  </p>
					<p>If a charity or individual approaches you regarding fundraising, this is not from us. Feel free to give where you see fit, but please know that we do not ask for any additional money or donations for organizations.</p>

					<h4>Sistas In Soccer Mission Statement</h4>
					<p>It is the goal of Sistas In Soccer to provide a place for women of all ages to play the game of soccer in a fun and recreational setting, to meet new people, to get some exercise and to do it in a manner that is both kind and cordial to all other members of the league. Our mission is not to fundraise, create a competitive atmosphere, to cause harm or to undermine other players or leagues.
					Every year we have made changes to the league as it has stretched and grown. We have really appreciated the continued support of you, our players, and want to continue to offer a fun soccer experience at the recreational level. </p>

					<p>Sincerely, <br/>
					Erin, Tammy, and Chris</p>
				@endif

				@if($sessions->location->id == 3)
					<h4>SEASON START</h4>
					<p>The season will start at the beginning of June.  Games will be played Sundays between 7 and 9 pm.</p>
					<p>We will not schedule any games on the long weekends, so there will be no games on Victoria Day weekend, Canada Day weekend, Civic Holiday weekend and Labour Day weekend. The regular season will run to the end of August.  </p>
					<p>Once the schedule is made, you will be notified by email and can download it from the website. </p>

					<h4>YEAR-END TOURNAMENT</h4>
					<p>The year-end tournament will be after the regular season finishes – we’re hoping for a weekend near the beginning of September, depending on field availability, but we’ll keep you posted as the season progresses.</p>

					<h4>SISSY OF THE YEAR</h4>
					<p>Our goal is fun first, so we like to highlight the importance of this with the Sissy of the Game nomination. </p>
					<p>For those of you who are new to us this season, this is an award held in high esteem. For every game played, each team will nominate one player from their team to be the Sissy. This individual will have best exemplified a positive attitude and encouraging spirit during the game. This is not necessarily the most skilled player on the team. At the end of the season, the nominations are tallied and one player from each team wins an award. She who has the most fun wins!</p>

					<h4>CAPTAINS, PLAYERS, AND COACHES</h4>
					<p>There will be the Captains, Players and Coaches meeting held at RDAC Sports Complex before the season begins.  This will be a general meeting to answer questions and everyone is invited. </p>
					<p>It is imperative that the captains (and coaches if your team has one) from each team be at this meeting or sends a representative. This is when a lot of critical information is given out, as well as all the team equipment, including balls, clipboard, jerseys etc. </p>

					<h4>MISCELLANEOUS BITS</h4>
					<p>For insurance purposes, only registered Sistas in Soccer players can play in the league.  All players must be on your team roster, not from another Sistas team.  Our insurance coverage requires that each player have soccer cleats (not runners) and shin guards for every game. It is imperative that you have your equipment for each game or you will not be allowed to play. </p>
					<p>If a charity or individual approaches you regarding fundraising, this is not from us. Feel free to give where you see fit, but please know that we do not ask for any additional money or donations for organizations.</p>

					<h4>Sistas In Soccer Mission Statement</h4>
					<p>It is the goal of Sistas In Soccer to provide a place for women of all ages to play the game of soccer in a fun and recreational setting, to meet new people, to get some exercise and to do it in a manner that is both kind and cordial to all other members of the league. Our mission is not to fundraise, create a competitive atmosphere, to cause harm or to undermine other players or leagues.</p>
					<p>We really appreciate the support of you, our players, and want to offer a fun soccer experience at the recreational level. </p>

					<p>Sincerely, <br/>
					Erin, Tammy and Chris</p>
				@endif

				@if($sessions->location->id == 4)
					<h4>SEASON START</h4>
					<p>The season will start after Victoria Day weekend.  Games will be played Sundays, between 3 and 9 pm.</p>
					<p>We will not schedule any games on the long weekends, so there will be no games on Victoria Day weekend, Canada Day weekend, Civic Holiday weekend and Labour Day weekend. The regular season will run to the end of August.</p>
					<p>Once the schedule is made, you will be notified by email and can download it from the website.</p>

					<h4>YEAR-END TOURNAMENT</h4>
					<p>The year-end tournament will be after the regular season finishes – we’re hoping for a weekend at the end of August, depending on field availability, but we’ll keep you posted as the season progresses.</p>

					<h4>SISSY OF THE YEAR</h4>
					<p>Our goal is fun first, so we like to highlight the importance of this with the Sissy of the Game nomination.</p>
					<p>For those of you who are new to us this season, this is an award held in high esteem. For every game played, each team will nominate one player from their team to be the Sissy. This individual will have best exemplified a positive attitude and encouraging spirit during the game. This is not necessarily the most skilled player on the team. At the end of the season, the nominations are tallied and one player from each team wins an award. She who has the most fun wins!</p>

					<h4>CAPTAINS, PLAYERS, AND COACHES</h4>
					<p>There will be the Captains, Players and Coaches meeting held before the season begins.  This will be a general meeting to answer questions and everyone is invited.  Location and date will be sent out to the captains.</p>
					<p>It is imperative that the captains (and coaches if your team has one) from each team be at this meeting or sends a representative. This is when a lot of critical information is given out, as well as all the team equipment, including balls, clipboard, jerseys etc. </p>

					<h4>MISCELLANEOUS BITS</h4>
					<p>For insurance purposes, only registered Sistas in Soccer players can play in the league.  All players must be on your team roster, not from another Sistas team.  Our insurance coverage requires that each player have soccer cleats (not runners) and shin guards for every game. It is imperative that you have your equipment for each game or you will not be allowed to play.  </p>
					<p>If a charity or individual approaches you regarding fundraising, this is not from us. Feel free to give where you see fit, but please know that we do not ask for any additional money or donations for organizations.</p>

					<h4>Sistas In Soccer Mission Statement</h4>
					<p>It is the goal of Sistas In Soccer to provide a place for women of all ages to play the game of soccer in a fun and recreational setting, to meet new people, to get some exercise and to do it in a manner that is both kind and cordial to all other members of the league. Our mission is not to fundraise, create a competitive atmosphere, to cause harm or to undermine other players or leagues. We really appreciate the support of you, our players, and want to offer a fun soccer experience at the recreational level. </p>

					<p>Sincerely, <br/>
					Teri Lynn</p>
				@endif

				@if($sessions->location->id == 5)
					<h4>SEASON START</h4>

					<p>The season will start after Victoria Day weekend.  Games will be played Sundays, between 3 and 9 pm.</p>
					<p>We will not schedule any games on the long weekends, so there will be no games on Victoria Day weekend, Canada Day weekend, Civic Holiday weekend and Labour Day weekend. The regular season will run to the end of August.</p>
					<p>The schedule will be made and posted by Victoria Day weekend.  You will be notified by email and can download it from the website.</p>

					<h4>TEAM ROSTER</h4>
					<p>Your team roster will be made and emailed to you in early May.  On it you will find the name and contact information for each person on your team.  If you have NOT received a roster two weeks before the first game of the season, please contact us!</p>

					<h4>YEAR-END TOURNAMENT</h4>
					<p>The year-end tournament will be after the regular season finishes – we’re hoping for a weekend near the end of September, depending on field availability, but we’ll keep you posted as the season progresses.</p>

					<h4>FIELDS AND GAME CANCELLATIONs</h4>
					<p>There are maps to the field locations available on our website and on the City of Hamilton website.  Because the fields we use are city-owned, they make the call on field conditions due to weather and if the fields are too poor to play on.  PLEASE check this website to find out about field conditions:
					https://www.hamilton.ca/parks-recreation/facility-rentals-permits/sports-fields/rain-out-policy</p>

					<h4>SISSY OF THE YEAR</h4>
					<p>Our goal is fun first, so we like to highlight the importance of this with the Sissy of the Game nomination.</p>
					<p>For those of you who are new to us this season, this is an award held in high esteem. For every game played, each team will nominate one player from their team to be the Sissy. This individual will have best exemplified a positive attitude and encouraging spirit during the game. This is not necessarily the most skilled player on the team. At the end of the season, the nominations are tallied and one player from each team wins an award. She who has the most fun wins!</p>

					<h4>CAPTAINS, PLAYERS, AND COACHES</h4>
					<p>There will be the Captains, Players and Coaches meeting held before the season begins.  This will be a general meeting to answer questions and everyone is invited.  Location and date will be sent out to the captains.</p>
					<p>It is imperative that the captains (and coaches if your team has one) from each team be at this meeting or sends a representative. This is when a lot of critical information is given out, as well as all the team equipment, including balls, clipboard, jerseys etc.</p>

					<h4>MISCELLANEOUS BITS</h4>
					<p>-For insurance purposes, only registered Sistas in Soccer players can play in the league.  All players must be on your team roster, not from another Sistas team.  Our insurance coverage requires that each player have soccer cleats (not runners) and shin guards for every game. It is imperative that you have your equipment for each game or you will not be allowed to play.</p>
					<p>-If a charity or individual approaches you regarding fundraising, this is not from us. Feel free to give where you see fit, but please know that we do not ask for any additional money or donations for organizations.</p>

					<h4>Sistas In Soccer Mission Statement</h4>
					<p>It is the goal of Sistas In Soccer to provide a place for women of all ages to play the game of soccer in a fun and recreational setting, to meet new people, to get some exercise and to do it in a manner that is both kind and cordial to all other members of the league. Our mission is not to fundraise, create a competitive atmosphere, to cause harm or to undermine other players or leagues.</p>
					We really appreciate the support of you, our players, and want to offer a fun soccer experience at the recreational level.</p>

					<p>Sincerely,<br/>
					Karen</p>

				@endif-->

				<input type="hidden" name="id" value="{{ $registration->id }}">
				<button>Agree</button>
			{{ Form::close() }}
		</div>
	</div>

@stop
