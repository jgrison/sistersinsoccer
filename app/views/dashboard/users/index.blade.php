@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
		Manage Users
		</div>
		<div class="button">
		</div>
	</div>
</div>
<div class="page">
	<div id="users">
		<ul class="list">
			<li class="title">
				<div class="name">Name</div>
				<div class="email">Email</div>
				<div class="phone">Phone</div>
				<div class="cell">Cell</div>
			</li>
		@foreach($users as $user)
			<li>
				<div class="name">{{ $user->first_name }} {{ $user->last_name }}</div>
				<div class="email">{{ $user->email }}</div>
				<div class="phone">@if($user->phone){{ $user->phone }}@else - @endif</div>
				<div class="cell">@if($user->cell){{ $user->cell }}@else - @endif</div>
				<div class="manage">
					<a href="{{ URL::to('dashboard/user/edit/'.$user->id) }}">Edit</a>
					<span>|</span>
					<a class="delete" href="{{ URL::to('dashboard/user/delete/'.$user->id) }}">Delete</a>
				</div>
			</li>
		@endforeach
		</ul>
		{{ $users->links() }}
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#users .list li .delete').click(function() {
			return confirm("Are you sure you want to delete?");
		});
	});
</script>
@stop
