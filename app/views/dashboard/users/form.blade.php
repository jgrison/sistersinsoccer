@extends('master.dashboard')

@section('content')
<div class="page-title">
	<div class="content">
		<div class="text">
		{{ $user->first_name }} {{ $user->last_name }}
		</div>
		<div class="button">
		</div>
	</div>
</div>
<div class="page">
	<div id="users">
		{{ Form::open(array('url' => 'dashboard/user/edit_post', 'files' => true)) }}
			<input type="hidden" name="id" value="{{ $user->id }}">
			<div class="input">
				<h5>First Name</h5>
				<input name="first_name" placeholder="First Name" value="{{ $user->first_name }}">
			</div>
			<div class="input">
				<h5>Last Name</h5>
				<input name="last_name" placeholder="Last Name" value="{{ $user->last_name }}">
			</div>
			<div class="input clear">
				<h5>Email</h5>
				<input name="email" placeholder="Email" value="{{ $user->email }}">
			</div>
			<div class="input">
				<h5>Date of Birth</h5>
				<input name="dob" placeholder="Date Of Birth" value="{{ $user->dob }}">
			</div>
			<div class="input">
				<h5>Phone</h5>
				<input name="phone" placeholder="Phone" value="{{ $user->phone }}">
			</div>
			<div class="input clear">
				<h5>Cell</h5>
				<input name="cell" placeholder="Cell" value="{{ $user->cell }}">
			</div>
			<div class="input">
				<h5>Address</h5>
				<input name="address" placeholder="Address" value="{{ $user->address }}">
			</div>
			<div class="input">
				<h5>City</h5>
				<input name="city" placeholder="City" value="{{ $user->city }}">
			</div>
			<div class="input clear">
				<h5>Province</h5>
				<select name="province">
					<option value="">Province</option>
					<option value="AB" @if($user->province == "AB") selected="selected" @endif>AB</option>
					<option value="BC" @if($user->province == "BC") selected="selected" @endif>BC</option>
					<option value="MB" @if($user->province == "MB") selected="selected" @endif>MB</option>
					<option value="NB" @if($user->province == "NB") selected="selected" @endif>NB</option>
					<option value="NL" @if($user->province == "NL") selected="selected" @endif>NL</option>
					<option value="NS" @if($user->province == "NS") selected="selected" @endif>NS</option>
					<option value="NT" @if($user->province == "NT") selected="selected" @endif>NT</option>
					<option value="NU" @if($user->province == "NU") selected="selected" @endif>NU</option>
					<option value="ON" @if($user->province == "ON") selected="selected" @endif>ON</option>
					<option value="PE" @if($user->province == "PE") selected="selected" @endif>PE</option>
					<option value="QC" @if($user->province == "QC") selected="selected" @endif>QC</option>
					<option value="SK" @if($user->province == "SK") selected="selected" @endif>SK</option>
					<option value="YT" @if($user->province == "YT") selected="selected" @endif>YT</option>
				</select>
			</div>
			<div class="input">
				<h5>Postal Code</h5>
				<input name="postal_code" placeholder="Postal Code" value="{{ $user->postal_code }}">
			</div>
			<br style="clear:both;"/>
			<div class="bottom">
				<button>Edit User</button>
			</div>
		{{ Form::close() }}
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#teams .list li .delete').click(function() {
			return confirm("Are you sure you want to delete?");
		});
	});
</script>
@stop
