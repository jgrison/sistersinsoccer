<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Sistas in Soccer</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="{{ asset('img/fav.png') }}"/>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/main.css') }}">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    </head>
    <body>
		@if(Session::has('success'))
		<div class="alert success">
			{{ Session::get('success') }}
			<div class="dismiss">x</div>
		</div>
		@endif

		@if(Session::has('error'))
		<div class="alert error">
			{{ Session::get('error') }}
			<div class="dismiss">x</div>
		</div>
		@endif

		<div id="header">
			<div class="logo">
				<img src="{{ asset('img/logo.jpg') }}">
			</div>
			<div class="title">
				<h1>Registration</h1>
			</div>
		</div>

		<div class="content">
        	@yield('content')
		</div>

		<script src="{{ asset('js/main.js') }}"></script>
	</body>
</html>
