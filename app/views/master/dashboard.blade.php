<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Sistas in Soccer | Dashboard</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/png" href="{{ asset('img/fav.png') }}"/>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="{{ asset('js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    </head>
    <body>
		@if(Session::has('success'))
		<div class="alert success">
			{{ Session::get('success') }}
			<div class="dismiss">x</div>
		</div>
		@endif

		@if(Session::has('error'))
		<div class="alert error">
			{{ Session::get('error') }}
			<div class="dismiss">x</div>
		</div>
		@endif

        <div id="header">
            <div class="content">
                <div class="logo">
                    <img src="{{ asset('img/logo.png') }}">
                </div>
                <ul class="navigation">
                    <li><a href="{{ URL::to('dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ URL::to('dashboard/sessions') }}">Sessions</a></li>
                    <li><a href="{{ URL::to('dashboard/teams') }}">Teams</a></li>
                    <li><a href="{{ URL::to('dashboard/fields') }}">Fields</a></li>
                    <li><a href="{{ URL::to('dashboard/schedules') }}">Scheduling</a></li>
                    <li><a href="{{ URL::to('dashboard/users') }}">Users</a></li>
                </ul>
            </div>
        </div>

        @yield('content')

        <script src="{{ asset('js/datepicker.js') }}"></script>
		<script src="{{ asset('js/main.js') }}"></script>
	</body>
</html>
