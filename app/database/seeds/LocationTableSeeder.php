<?php

class LocationTableSeeder extends Seeder {

	public function run()
	{
		$locations = ["London", "Guelph", "Windsor", "Hamilton"];

		foreach($locations as $location)
		{
			$exists = Locations::where('name', $location)->first();

			if(!$exists)
			{
				$new = new Locations();
				$new->name = $location;

				$new->save();
			}
		}
	}

}
