<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		$user = User::where('email', 'rob@reddingdesigns.com')->first();

		if(!$user)
		{
			$user = new User();
			$user->password = Hash::make("Redding$16");
			$user->email = "rob@reddingdesigns.com";
			$user->first_name = "Rob";
			$user->last_name = "Redding";
			$user->admin = 1;

			$user->save();
		}
	}

}
