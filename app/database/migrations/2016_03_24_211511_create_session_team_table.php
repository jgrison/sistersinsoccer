<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionTeamTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('session_teams', function(Blueprint $table)
      	{
			$table->increments('id');
			$table->integer('session_id');
			$table->integer('team_id');
			$table->integer('user_id');
			$table->integer('captain')->nullable();
			$table->integer('goalie')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('session_teams');
	}

}
