<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleTeamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schedule_teams', function(Blueprint $table)
      	{
			$table->increments('id');
			$table->integer('schedule_id');
			$table->integer('field_id');
			$table->integer('home');
			$table->integer('away');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('schedule_teams');
	}

}
