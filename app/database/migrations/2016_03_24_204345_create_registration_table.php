<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('registration', function(Blueprint $table)
      	{
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('session_id');
			$table->text('position')->nullable();
			$table->integer('goalie')->nullable();
			$table->integer('captain')->nullable();
			$table->text('code')->nullable();
			$table->integer('waiver')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registration');
	}

}
