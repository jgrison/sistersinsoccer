<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
      	{
			$table->increments('id');
			$table->text('email');
			$table->text('password');
			$table->text('first_name')->nullable();
			$table->text('last_name')->nullable();
			$table->text('dob')->nullable();
			$table->text('phone')->nullable();
			$table->text('cell')->nullable();
			$table->text('address')->nullable();
			$table->text('city')->nullable();
			$table->text('province')->nullable();
			$table->text('postal_code')->nullable();
			$table->integer('admin')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
