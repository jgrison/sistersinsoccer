<?php

class SessionTeams extends Eloquent {

    protected $table = 'session_teams';

    public function player()
    {
        $player = User::find($this->user_id);

        if($player)
        {
            return $player;
        }

        return "";
    }
}
