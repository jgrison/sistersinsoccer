<?php

class Locations extends Eloquent {

    protected $table = 'locations';

	public function sessions()
	{
		return $this->hasMany('Sessions', 'location_id');
	}

    public function fields()
    {
        return $this->hasMany('Field', 'location_id');
    }
}
