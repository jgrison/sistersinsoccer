<?php

class Payments extends Eloquent {

    protected $table = 'payments';

	public function registration()
	{
		return $this->belongsTo('Registration');
	}
}
