<?php

class Schedule extends Eloquent {

    protected $table = 'schedule';

	public function session()
	{
		return $this->belongsTo('Sessions');
	}

	public function games()
	{
		return $this->hasMany('ScheduleTeam');
	}
}
