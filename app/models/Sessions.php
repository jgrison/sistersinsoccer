<?php

class Sessions extends Eloquent {

    protected $table = 'sessions';

	public function registrations()
	{
		return $this->hasMany('Registration', 'session_id');
	}

	public function location()
	{
		return $this->belongsTo('Locations');
	}

    public function teams()
    {
        return $this->hasMany('Team', 'session_id');
    }

    public function hasSchedule()
    {
        $schedule = Schedule::where('session_id', $this->id)->first();

        if($schedule)
        {
            return true;
        }

        return false;
    }
}
