<?php

class ScheduleTeam extends Eloquent {

    protected $table = 'schedule_teams';

	public function schedule()
	{
		return $this->belongsTo('Schedule');
	}

	public function team()
	{
		return $this->belongsTo('Team');
	}

    public function field()
	{
		return $this->belongsTo('Field');
	}

    public function getHomeTeam()
    {
        $team = Team::find($this->home);

        if($team)
        {
            return $team;
        }

        return '';
    }

    public function getAwayTeam()
    {
        $team = Team::find($this->away);

        if($team)
        {
            return $team;
        }

        return '';
    }
}
