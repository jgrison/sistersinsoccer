<?php

class Team extends Eloquent {

    protected $table = 'teams';

	public function session()
	{
		return $this->belongsTo('Sessions');
	}

    public function schedule()
	{
		return $this->hasMany('ScheduleTeam');
	}

    public function getTeamPlayers($session_id)
    {
        $players = SessionTeams::where('session_id', $session_id)->where('team_id', $this->id)->get();

        if($players)
        {
            return $players;
        }

        return "d";
    }

    public function getConfig($attribute)
    {
        $item = TeamConfig::where('team_id', $this->id)->where('option', $attribute)->first();

        if($item)
        {
            return $item->value;
        }

        return "";
    }
}
