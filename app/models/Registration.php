<?php

class Registration extends Eloquent {

    protected $table = 'registration';

	public function session()
	{
		return $this->belongsTo('Sessions');
	}

	public function user()
	{
		return $this->belongsTo('User');
	}

    public function getTeam()
    {
        $team = SessionTeams::where('user_id', $this->user_id)->where('session_id', $this->session_id)->first();

        if($team)
        {
            return $team;
        }

        return "";
    }
}
