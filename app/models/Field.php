<?php

class Field extends Eloquent {

    protected $table = 'fields';

    public function location()
    {
        return $this->belongsTo('Locations');
    }

    public function game()
    {
        return $this->belongsTo('ScheduleTeam', 'field_id');
    }
}
