<?php

class UserController extends BaseController {

	public function index()
	{
		$users = User::paginate(25);
		return View::make('dashboard.users.index')->with('users', $users);
	}

	public function create()
	{
		return View::make('dashboard.users.form');
	}

	public function createPost()
	{
		$rules = array(
			'email'    => 'required',
			'password' => 'required|min:3',
			'first_name' => 'required',
			'last_name' => 'required',
			'phone' => 'required',
			'address' => 'required',
			'city' => 'required',
			'province' => 'required',
			'postal_code' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$exists = User::where('email', Input::get('email'))->first();

			if(Input::get('password') != Input::get('confirm_password'))
			{
				return Redirect::back()->with("error", "Please make sure your passwords match.");
			}

			if(!$exists)
			{
				$user = new User();
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->first_name = Input::get('first_name');
				$user->last_name = Input::get('last_name');
				$user->dob = Input::get('dob');
				$user->phone = Input::get('phone');
				$user->cell = Input::get('cell');
				$user->address = Input::get('address');
				$user->city = Input::get('city');
				$user->province = Input::get('province');
				$user->postal_code = Input::get('postal_code');

				if($user->save())
				{
					return Redirect::to('dashboard');
				}

				return Redirect::back()->with("error", "There was a problem registering you.");
			}

			return Redirect::back()->with("error", "An account for that email already exists.");
		}

		return Redirect::back()->with("error", "Make sure you fill out the form completely.");
	}

	public function edit($id)
	{
		$user = User::find($id);

		if($user)
		{
			return View::make('dashboard.users.form')->with('user', $user);
		}

		return Redirect::back()->with("error", "That User Doesn't Exist.");
	}

	public function editPost()
	{
		$rules = array(
			'id' => 'required',
			'email'    => 'required',
			'first_name' => 'required',
			'last_name' => 'required',
			'phone' => 'required',
			'address' => 'required',
			'city' => 'required',
			'province' => 'required',
			'postal_code' => 'required',
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$user = User::find(Input::get('id'));

			if($user)
			{
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->first_name = Input::get('first_name');
				$user->last_name = Input::get('last_name');
				$user->dob = Input::get('dob');
				$user->phone = Input::get('phone');
				$user->cell = Input::get('cell');
				$user->address = Input::get('address');
				$user->city = Input::get('city');
				$user->province = Input::get('province');
				$user->postal_code = Input::get('postal_code');

				if($user->save())
				{
					return Redirect::back()->with("success", "Successfully edited your account.");
				}
			}

			return Redirect::back()->with("error", "That user doesn't exist.");
		}

		return Redirect::back()->with("error", "Make sure you fill out the form completely.");
	}

	public function delete($id)
	{
		$user = User::find($id);

		if($user)
		{
			$registrations = Registration::where('user_id', $user->id)->get();
			$teams = SessionTeams::where('user_id', $user->id)->get();

			foreach($registrations as $registration)
			{
				try
				{
					$registration->delete();
				}
				catch(Exception $e)
				{
					return Redirect::back()->with("error", "There was an error deleting that user");
				}
			}

			foreach($teams as $team)
			{
				try
				{
					$team->delete();
				}
				catch(Exception $e)
				{
					return Redirect::back()->with("error", "There was an error deleting that user");
				}
			}

			if($user->delete())
			{
				return Redirect::back()->with("success", "That user has been successfully deleted!");
			}
		}

		return Redirect::back()->with("error", "That user doesn't exist");
	}
}
