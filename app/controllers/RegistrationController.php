<?php

class RegistrationController extends BaseController {

	public function create()
	{
		$data['locations'] = Locations::all();
		$data['sessions'] = Sessions::where('active', 1)->get();

		return View::make('dashboard.registration.form', $data);
	}

	public function createPost()
	{
		$rules = array(
			'email'    => 'required',
			'password' => 'required|min:3',
			'first_name' => 'required',
			'last_name' => 'required',
			'phone' => 'required',
			'address' => 'required',
			'city' => 'required',
			'province' => 'required',
			'postal_code' => 'required',
			'session' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$exists = User::where('email', Input::get('email'))->first();

			if(Input::get('password') != Input::get('confirm_password'))
			{
				return Redirect::back()->with("error", "Please make sure your passwords match.");
			}

			if($exists)
			{
				$registration_exists = Registration::where('user_id', Input::get('id'))->with('session_id', Input::get('session'))->first();

				if(!$registration_exists)
				{
					$registration = new Registration();
					$registration->user_id = $exists->id;
					$registration->session_id = Input::get('session');
					$registration->position = Input::get('position');
					$registration->goalie = Input::get('goalie');
					$registration->captain = Input::get('captain');
					$registration->code = Input::get('code');

					if($registration->save())
					{
						return Redirect::to('register/waiver/'.$registration->id);
					}
				}

				return Redirect::back()->with("error", "You're already registered for that session.");
			}
			else
			{
				$user = new User();
				$user->email = Input::get('email');
				$user->password = Hash::make(Input::get('password'));
				$user->first_name = Input::get('first_name');
				$user->last_name = Input::get('last_name');
				$user->dob = Input::get('dob');
				$user->phone = Input::get('phone');
				$user->cell = Input::get('cell');
				$user->address = Input::get('address');
				$user->city = Input::get('city');
				$user->province = Input::get('province');
				$user->postal_code = Input::get('postal_code');

				if($user->save())
				{
					$registration = new Registration();
					$registration->user_id = $user->id;
					$registration->session_id = Input::get('session');
					$registration->position = Input::get('position');
					$registration->goalie = Input::get('goalie');
					$registration->captain = Input::get('captain');
					$registration->code = Input::get('code');

					if($registration->save())
					{
						return Redirect::to('register/waiver/'.$registration->id);
					}
				}

				return Redirect::back()->with("error", "There was a problem registering you.");
			}
		}

		return Redirect::back()->with("error", "Make sure you fill out the form completely.");
	}

	public function waiver($id)
	{
		$data['registration'] = Registration::find($id);

		if($data['registration'])
		{
			$data['sessions'] = Sessions::find($data['registration']->session_id);
			return View::make('dashboard.registration.waiver', $data);
		}

		return Redirect::back()->with("error", "There's no registration for that session");
	}

	public function waiverPost()
	{
		$registration = Registration::find(Input::get('id'));

		if($registration)
		{
			$registration->waiver = 1;

			if($registration->save())
			{
				return Redirect::to('register/payments/'.$registration->id);
			}

			return Redirect::back()->with("error", "Problem saving waiver.");
		}

		return Redirect::back()->with("error", "There's no registration for that session");
	}

	public function payments($id)
	{
		$data['registration'] = Registration::find($id);

		if($data['registration'])
		{
			$data['session'] = Sessions::find($data['registration']->session_id);
			return View::make('dashboard.registration.payment', $data);
		}

		return Redirect::back()->with("error", "There's no registration for that session");
	}

	public function paymentsPost()
	{
		$rules = array(
			'card'    => 'required',
			'cvc' => 'required',
			'exp_month' => 'required',
			'exp_year' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$exists = Payments::where('registration_id', Input::get('id'))->first();

			if(!$exists)
			{
				$decimal = explode('.', Input::get('amount'));

				if(count($decimal) > 1)
					$price = ((int)$decimal[0] * 100) + (int)$decimal[1];
				else
					$price = ((int)$decimal[0] * 100);

				\Stripe\Stripe::setApiKey('sk_live_IyujJ6yYs6US5S5GDkE3xriq');
				$myCard = array('number' => Input::get('card'), 'cvc' => Input::get('cvc'), 'exp_month' => Input::get('exp_month'), 'exp_year' => Input::get('exp_year'));

				try
				{
					$charge = \Stripe\Charge::create(array('card' => $myCard, 'amount' => $price, 'currency' => 'cad'));
				}
				catch(\Stripe\Error\Card $e)
				{
					return Redirect::back()->with('error', "Your Credit Card is Incorrect.");
				}

				if ($charge->status == "succeeded")
				{
					$payment = new Payments();
					$payment->registration_id = Input::get('id');
					$payment->stripe_id = $charge->id;
					$payment->balance_transaction = $charge->balance_transaction;
					$payment->amount = Input::get('amount');

					if($payment->save())
					{
						$data['registration'] = Registration::find(Input::get('id'));
						$data['session'] = Sessions::find($data['registration']->session_id);
						$data['user'] = User::find($data['registration']->user_id);

						// Send email to customer
						if($data['user'] && $data['user']->email)
						{
							$email = $data['user']->email;
							Mail::send('emails.register.receipt', $data, function ($message) use ($email) {
								$message->from('signup@sistasregistration.com', 'Sistas In Soccer');
								$message->to($email)->subject('Your Registration & Reciept');
							});
						}

						// Send email to Sistas
						Mail::send('emails.register.new', $data, function ($message) {
							$message->from('signup@sistasregistration.com', 'Sistas In Soccer');
							$message->to('registrations@sistasinsoccer.com')->subject('New Registration & Payment');
						});

						// Redirect
						return Redirect::to('register/thanks');
					}
				}
			}

			return Redirect::back()->with("error", "We've already received your payment for this season!");
		}

		return Redirect::back()->with("error", "Error Processing Credit Card");
	}

	public function thanks()
	{
		return View::make('dashboard.registration.thanks');
	}

	public function delete($id)
	{
		$registration = Registration::find($id);

		if($registration)
		{
			if($registration->delete())
			{
				return Redirect::back()->with("success", "Registration successfully deleted.");
			}

			return Redirect::back()->with("error", "Problem deleting registration.");
		}

		return Redirect::back()->with("error", "Registration doesn't exist.");
	}
	public function test()
	{
		// $users = User::all();
		//
		// foreach($users as $user)
		// {
		// 	echo $user->email.'<br/>';
		// 	foreach($user->registrations as $registration)
		// 	{
		// 		if(count($registration->payment) > 1)
		// 		{
		// 			echo $registration->payment.'<br/>';
		// 		}
		// 	}
		//
		// 	echo '<hr/>';
		// }


		// return View::make('emails.register.receipt', $data);
	}

}
