<?php

class SessionsController extends BaseController {

	public function index()
	{
		$data['sessions'] = Sessions::orderBy('active', 'DESC')->orderBy('created_at', 'DESC')->get();
		$data['locations'] = Locations::all();
		return View::make('dashboard.sessions.index', $data);
	}

	public function view($id)
	{
		$session = Sessions::find($id);

		if($session)
		{
			return View::make('dashboard.sessions.view')->with("session", $session);
		}

		return Redirect::to('dashboard/sessions')->with("error", "That session doesn't exist.");
	}

	public function create()
	{
		$locations = Locations::all();
		return View::make('dashboard.sessions.form')->with('locations', $locations);
	}

	public function createPost()
	{
		$rules = array(
		    'name'    => 'required',
		    'location' => 'required',
			'price' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$exists = Sessions::where('name', Input::get('name'))->where('location_id', Input::get('location'))->first();

			if(!$exists)
			{
				$session = new Sessions();
				$session->location_id = Input::get('location');
				$session->name = Input::get('name');
				$session->price = Input::get('price');
				$session->start_date = Input::get('start_date');
				$session->end_date = Input::get('end_date');
				$session->active = 1;

				if($session->save())
				{
					return Redirect::to('dashboard/sessions')->with("success", "Successfully created new Session.");
				}

				return Redirect::to()->with("error", "Problem saving session");
			}

			return Redirect::back()->with("error", "That session already exists.");
		}

		return Redirect::back()->with("error", "Please make sure you filled out the form completely.");
	}

	public function edit($id)
	{
		$data['session'] = Sessions::find($id);
		$data['locations'] = Locations::all();

		if($data['session'])
		{
			return View::make('dashboard.sessions.form', $data);
		}

		return Redirect::back()->with("error", "That session doesn't exists.");
	}

	public function editPost()
	{
		$rules = array(
		    'name'    => 'required',
		    'location' => 'required',
			'price' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$session = Sessions::find(Input::get('id'));

			if($session)
			{
				$session->location_id = Input::get('location');
				$session->name = Input::get('name');
				$session->price = Input::get('price');
				$session->start_date = Input::get('start_date');
				$session->end_date = Input::get('end_date');

				if($session->save())
				{
					return Redirect::to('dashboard/sessions')->with("success", "Successfully edited Session.");
				}

				return Redirect::to()->with("error", "Problem saving session");
			}

			return Redirect::back()->with("error", "That session doesn't exists.");
		}

		return Redirect::back()->with("error", "Please make sure you filled out the form completely.");
	}

	public function delete($id)
	{
		$session = Sessions::find($id);

		if($session)
		{
			if($session->delete())
			{
				return Redirect::back()->with("success", "Successfully deleted session.");
			}

			return Redirect::back()->with("error", "There was a problem deleting that session.");
		}

		return Redirect::back()->with("error", "That session doesn't exists.");
	}

	public function status($id)
	{
		$session = Sessions::find($id);

		if($session)
		{
			$session->active = 0;

			if($session->save())
			{
				return Redirect::back()->with("success", "Session successfully disabled.");
			}

			return Redirect::back()->with("error", "Problem disabling session.");
		}

		return Redirect::back()->with("error", "Session doesn't exist.");
	}

	public function roster($id)
	{
		$session = Sessions::find($id);

		if($session)
		{
			return View::make('dashboard.sessions.roster')->with("session", $session);
		}

		return Redirect::to('dashboard/sessions')->with("error", "That session doesn't exist.");
	}

	public function teams($id)
	{
		$data['team'] = Team::find($id);
		$data['session'] = Sessions::find(Request::get('id'));
		return View::make('dashboard.sessions.teams', $data);
	}

	public function enroll()
	{
		$exists = SessionTeams::where('user_id', Input::get('user'))->where('session_id', Input::get('session'))->first();

		if($exists)
		{
			if(Input::get('team') == "unassign")
			{
				$exists->delete();
				return "Success";
			}

			if($exists->team_id != Input::get("team"))
			{
				$exists->team_id = Input::get("team");

				if($exists->save())
				{
					return "Success";
				}
			}
		}
		else
		{
			if(Input::get('team') != "unassign")
			{
				$enroll = new SessionTeams();
				$enroll->user_id = Input::get('user');
				$enroll->session_id = Input::get('session');
				$enroll->team_id = Input::get('team');

				if($enroll->save())
				{
					return "Success";
				}
			}
		}
	}
}
