<?php

class FieldController extends BaseController {

	public function index()
	{
		$data['fields'] = Field::orderBy('location_id', 'ASC')->get();
		$data['locations'] = Locations::all();
		return View::make('dashboard.fields.index', $data);
	}

	public function create()
	{
		$locations = Locations::all();
		return View::make('dashboard.fields.form')->with('locations', $locations);
	}

	public function createPost()
	{
		$rules = array(
		    'name'    => 'required',
		    'location' => 'required',
			'address' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$exists = Field::where('location_id', Input::get('location'))->where('name', Input::get('name'))->first();

			if(!$exists)
			{
				$field = new Field();
				$field->location_id = Input::get('location');
				$field->name = Input::get('name');
				$field->address = Input::get('address');

				if($field->save())
				{
					return Redirect::to('dashboard/fields')->with("success", "Field Successfully Added");
				}
			}

			return Redirect::back()->with("error", "That field already exists.");
		}

		return Redirect::back()->with("error", "Make sure you fill out the form completely");
	}

	public function edit($id)
	{
		$data['field'] = Field::find($id);
		$data['locations'] = Locations::all();

		if($data['field'])
		{
			return View::make('dashboard.fields.form', $data);
		}

		return Redirect::back()->with("error", "That field doesn't exist");
	}

	public function editPost()
	{
		$rules = array(
		    'name'    => 'required',
		    'location' => 'required',
			'address' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$field = Field::find(Input::get('id'));

			if($field)
			{
				$field->location_id = Input::get('location');
				$field->name = Input::get('name');
				$field->address = Input::get('address');

				if($field->save())
				{
					return Redirect::to('dashboard/fields')->with("success", "Field Successfully Updated");
				}
			}

			return Redirect::back()->with("error", "That field doesn't exist.");
		}

		return Redirect::back()->with("error", "Make sure you fill out the form completely");
	}

	public function delete($id)
	{
		$field = Field::find($id);

		if($field)
		{
			if($field->delete())
			{
				return Redirect::to('dashboard/fields')->with("success", "Successfully deleted field");
			}

			return Redirect::back()->with("error", "Error deleting that field.");
		}

		return Redirect::back()->with("error", "That Field doesn't exist");
	}
}
