<?php

class DashboardController extends BaseController {

	public function dashboard()
	{
		$registrations = Registration::all();
		return View::make('dashboard.dashboard.overview')->with('registrations', $registrations);
	}

	public function login()
	{
		return View::make('dashboard.dashboard.login');
	}

	public function loginPost()
	{
		$rules = array(
		    'email'    => 'required',
		    'password' => 'required|min:3'
		);

		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
		    return Redirect::to('login')->withInput(Input::except('password'))->with('error', 'Username or Password is incorrect');
		}
		else
		{
		    $userdata = array(
		        'email'  => Input::get('email'),
		        'password'  => Input::get('password')
		    );

		    // Attempt Login
		    if (Auth::attempt($userdata))
		    {
				return Redirect::to('dashboard');
		    }

		    return Redirect::to('login')->withInput(Input::except('password'))->with('error', 'Username or Password is incorrect');
		}
	}

	public function logout()
	{
		Auth::logout();
		return Redirect::to('login');
	}

}
