<?php

class MessagingController extends BaseController {

	public function index()
	{
		$messages = Messaging::all();
		return View::make('dashboard.messaging.index')->with('messages', $messages);
	}

	public function create()
	{
		$data['sessions'] = Sessions::where('active', 1)->get();
		return View::make('dashboard.messaging.form', $data);
	}

	public function createPost()
	{
		if(Input::get('subject') && Input::get('message'))
		{
			$message = new Messaging();
			$message->subject = Input::get('subject');
			$message->body = Input::get('message');

			// If there's an attachment, upload it
			if(Input::hasFile('attachment'))
			{
				$filename = str_replace(' ', '', Input::file('attachment')->getClientOriginalName());

				if(file_exists(public_path().'/uploads/files/'.$filename))
				{
					$filename = str_random(3).'_'.$filename;
				}

				if(file_exists(public_path().'/uploads/files'))
				{
					Input::file('attachment')->move(public_path().'/uploads/files', $filename);
					$message->attachment = '/uploads/files/'.$filename;
				}
			}

			if($message->save())
			{
				// Send Messages
				$data['message'] = $message;

				$subject = $message->subject;
				$emails = ["jeffgrison@gmail.com", "jgrison@casive.com"];
				$attachment = asset($message->attachment);

				Mail::queue('emails.message', $data, function ($message) use ($emails, $subject) {
					$message->from('noreply@sistasregistration.com', 'Sistas In Soccer');
					$message->to($emails)->subject($subject);
					$message->attachment($attachment);
				});

				return Redirect::to('dashboard/messaging')->with("success", "Sucessfully Sent Message!");
			}

			return Redirect::back()->with("error", "There was a problem sending that message");
		}

		return Redirect::back()->with("error", "Please make sure you have a Subject and Message");
	}
}
