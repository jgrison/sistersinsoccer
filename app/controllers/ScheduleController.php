<?php

class ScheduleController extends BaseController {

	public function index()
	{
		$sessions = Sessions::all();
		return View::make('dashboard.schedules.index')->with('sessions', $sessions);
	}

	public function view($id)
	{
		$schedule = Schedule::where('session_id', $id)->get();
		return View::make('dashboard.schedules.view')->with('schedule', $schedule);
	}

	public function create()
	{
		if(Request::get('id'))
		{
			$data['session'] = Sessions::find(Request::get('id'));
			$data['teams'] = Team::where('location_id', $data['session']->location->id)->get();
			return View::make('dashboard.schedules.form', $data);
		}

		return View::make('dashboard.schedules.form');
	}

	public function createPost()
	{
		$rules = array(
			'teams'    => 'required',
			'fields'    => 'required',
			'total_games' => 'required',
			'start_date' => 'required',
			'end_date' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			// Generate dates in provided date range
			$dates = array();
			$start = strtotime(Input::get('start_date'));
			$end = strtotime(Input::get('end_date'));

			$schedule = [];
			$fields = explode(',', Input::get('fields'));
			$teams = explode(',', Input::get('teams'));
			$count = 0;

			while($start <= $end)
			{
				if(date("l", $start) == Input::get('days'))
				{
					$dates[] = date("d-m-Y", $start);
				}

				$start = strtotime("+1 day", $start);
			}

			// Generate Schedule
			foreach($dates as $date)
			{
				if($count != (int)Input::get('total_games'))
				{
					$new = new Schedule();
					$new->session_id = Input::get('id');
					$new->date = $date;
					$new->time = Input::get('time');

					if($new->save())
					{
						foreach($fields as $field)
						{
							if($field != "")
							{
								$teams = explode(',', Input::get('teams'));
								array_pop($teams);

								if(count($teams) > 0)
								{
									$exist = true;

									while($exist) {
										$random_one = rand(0, count($teams) - 1);
										$random_two = rand(0, count($teams) - 1);

										$home = $teams[$random_one];
										$away = $teams[$random_two];

										$check_home = ScheduleTeam::where('schedule_id', $new->id)->where('home', $home)->first();
										$check_home_away = ScheduleTeam::where('schedule_id', $new->id)->where('home', $away)->first();
										$check_away = ScheduleTeam::where('schedule_id', $new->id)->where('away', $away)->first();
										$check_away_home = ScheduleTeam::where('schedule_id', $new->id)->where('away', $home)->first();
										$check = ScheduleTeam::where('schedule_id', $new->id)->where('away', $away)->where('home', $home)->first();

										if($home == $away)
										{
											continue;
										}

										if(!$check && !$check_home && !$check_home_away && !$check_away & !$check_away_home)
										{
											$exist = false;
										}
									}

									unset($teams[$random_one]);
									unset($teams[$random_two]);

									$game = new ScheduleTeam();
									$game->schedule_id = $new->id;
									$game->field_id = $field;
									$game->home = $home;
									$game->away = $away;

									$game->save();
								}
							}
						}
					}

					$count++;
				}
			}

			return Redirect::to('dashboard/schedules')->with("success", "Successfully Generated Schedule.");
		}

		return Redirect::back()->with("error", "Please make sure you fill out the form fully.");
	}
}
