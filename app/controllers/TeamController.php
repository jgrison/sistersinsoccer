<?php

class TeamController extends BaseController {

	public function index()
	{
		$data['teams'] = Team::orderBy('session_id', 'ASC')->get();
		$data['sessions'] = Sessions::all();
		return View::make('dashboard.teams.index', $data);
	}

	public function create()
	{
		$locations = Locations::all();
		return View::make('dashboard.teams.form')->with('locations', $locations);
	}

	public function createPost()
	{
		$rules = array(
			'name'    => 'required',
			'session' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$exists = Team::where('name', Input::get('name'))->where('session_id', Input::get('session_id'))->first();

			if(!$exists)
			{
				$team = new Team();
				$team->session_id = Input::get('session');
				$team->name = Input::get('name');

				if($team->save())
				{
					return Redirect::to('dashboard/teams')->with("success", "Successfully created new team!");
				}

				return Redirect::back()->with("error", "Problem creating team");
			}

			return Redirect::back()->with("error", "That team already exists for that location");
		}

		return Redirect::back()->with("error", "Please make sure you fill out the form completely");
	}

	public function edit($id)
	{
		$data['team'] = Team::find($id);
		$data['sessions'] = Sessions::all();

		if($data['team'])
		{
			return View::make('dashboard.teams.form', $data);
		}

		return Redirect::back()->with("error", "That team doesn't exist.");
	}

	public function editPost()
	{
		$rules = array(
			'name'    => 'required',
			'session' => 'required'
		);

		$validator = Validator::make(Input::all(), $rules);

		if (!$validator->fails())
		{
			$team = Team::find(Input::get('id'));

			if($team)
			{
				$team->session_id = Input::get('session');
				$team->name = Input::get('name');

				if($team->save())
				{
					return Redirect::to('dashboard/teams')->with("success", "Successfully edited team!");
				}

				return Redirect::back()->with("error", "Problem creating team");
			}

			return Redirect::back()->with("error", "That team doesn't exist.");
		}

		return Redirect::back()->with("error", "Please make sure you fill out the form completely");
	}

	public function delete($id)
	{
		$team = Team::find($id);

		if($team)
		{
			if($team->delete())
			{
				return Redirect::back()->with("success", "Successfully deleted team.");
			}

			return Redirect::back()->with("error", "Problem deleting team");
		}

		return Redirect::back()->with("error", "That team doesn't exist");
	}

	public function assignCaptain()
	{
		$player = SessionTeams::where('session_id', Input::get('session'))->where('user_id', Input::get('user'))->where('team_id', Input::get('team'))->first();

		if($player)
		{
			if($player->captain == 1)
			{
				$player->captain = 0;
				$player->save();
				return "Success";
			}
			else
			{
				$player->captain = 1;
				$player->save();
				return "Success";
			}

			return "";
		}
	}

	public function assignGoalie()
	{
		$player = SessionTeams::where('session_id', Input::get('session'))->where('user_id', Input::get('user'))->where('team_id', Input::get('team'))->first();

		if($player)
		{
			if($player->goalie == 1)
			{
				$player->goalie = 0;
				$player->save();
				return "Success";
			}
			else
			{
				$player->goalie = 1;
				$player->save();
				return "Success";
			}

			return "";
		}
	}

}
