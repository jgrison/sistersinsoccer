<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Login Routes
Route::get('login', array('uses' => 'DashboardController@login'));
Route::post('login', array('uses' => 'DashboardController@loginPost'));
Route::get('logout', array('uses' => 'DashboardController@logout'));

// Dashboard
Route::get('dashboard', array('before' => 'auth', 'uses' => 'DashboardController@dashboard'));

// Sessions
Route::get('dashboard/sessions', array('before' => 'auth', 'uses' => 'SessionsController@index'));
Route::get('dashboard/sessions/view/{id}', array('before' => 'auth', 'uses' => 'SessionsController@view'));
Route::get('dashboard/sessions/roster/{id}', array('before' => 'auth', 'uses' => 'SessionsController@roster'));
Route::get('dashboard/sessions/teams/{id}', array('before' => 'auth', 'uses' => 'SessionsController@teams'));
Route::get('dashboard/sessions/create', array('before' => 'auth', 'uses' => 'SessionsController@create'));
Route::post('dashboard/sessions/create_post', array('before' => 'auth', 'uses' => 'SessionsController@createPost'));
Route::get('dashboard/sessions/edit/{id}', array('before' => 'auth', 'uses' => 'SessionsController@edit'));
Route::post('dashboard/sessions/edit_post', array('before' => 'auth', 'uses' => 'SessionsController@editPost'));
Route::get('dashboard/sessions/delete/{id}', array('before' => 'auth', 'uses' => 'SessionsController@delete'));
Route::get('dashboard/sessions/status/{id}', array('before' => 'auth', 'uses' => 'SessionsController@status'));
Route::post('dashboard/sessions/team/enroll', array('before' => 'auth', 'uses' => 'SessionsController@enroll'));

// Dashboard Registration Handling
Route::get('dashboard/registration/delete/{id}', array('before' => 'auth', 'uses' => 'RegistrationController@delete'));

// Fields
Route::get('dashboard/fields', array('before' => 'auth', 'uses' => 'FieldController@index'));
Route::get('dashboard/field/create', array('before' => 'auth', 'uses' => 'FieldController@create'));
Route::post('dashboard/field/create_post', array('before' => 'auth', 'uses' => 'FieldController@createPost'));
Route::get('dashboard/field/edit/{id}', array('before' => 'auth', 'uses' => 'FieldController@edit'));
Route::post('dashboard/field/edit_post', array('before' => 'auth', 'uses' => 'FieldController@editPost'));
Route::get('dashboard/field/delete/{id}', array('before' => 'auth', 'uses' => 'FieldController@delete'));

// Teams
Route::get('dashboard/teams', array('before' => 'auth', 'uses' => 'TeamController@index'));
Route::get('dashboard/teams/create', array('before' => 'auth', 'uses' => 'TeamController@create'));
Route::post('dashboard/teams/create_post', array('before' => 'auth', 'uses' => 'TeamController@createPost'));
Route::get('dashboard/teams/edit/{id}', array('before' => 'auth', 'uses' => 'TeamController@edit'));
Route::post('dashboard/teams/edit_post', array('before' => 'auth', 'uses' => 'TeamController@editPost'));
Route::get('dashboard/teams/delete/{id}', array('before' => 'auth', 'before' => 'auth', 'uses' => 'TeamController@delete'));
Route::post('dashboard/teams/captain/assign', array('before' => 'auth', 'uses' => 'TeamController@assignCaptain'));
Route::post('dashboard/teams/goalie/assign', array('before' => 'auth', 'uses' => 'TeamController@assignGoalie'));

// Scheduling
Route::get('dashboard/schedules', array('before' => 'auth', 'uses' => 'ScheduleController@index'));
Route::get('dashboard/schedule/view/{id}', array('before' => 'auth', 'uses' => 'ScheduleController@view'));
Route::get('dashboard/schedule/create', array('before' => 'auth', 'uses' => 'ScheduleController@create'));
Route::post('dashboard/schedule/create_post', array('before' => 'auth', 'uses' => 'ScheduleController@createPost'));

// Users
Route::get('dashboard/users', array('before' => 'auth', 'uses' => 'UserController@index'));
Route::get('dashboard/user/create', array('before' => 'auth', 'uses' => 'UserController@create'));
Route::post('dashboard/user/create_post', array('before' => 'auth', 'uses' => 'UserController@createPost'));
Route::get('dashboard/user/edit/{id}', array('before' => 'auth', 'uses' => 'UserController@edit'));
Route::post('dashboard/user/edit_post', array('before' => 'auth', 'uses' => 'UserController@editPost'));
Route::get('dashboard/user/delete/{id}', array('before' => 'auth', 'uses' => 'UserController@delete'));

// Messaging
Route::get('dashboard/messaging', array('before' => 'auth', 'uses' => 'MessagingController@index'));
Route::get('dashboard/messaging/create', array('before' => 'auth', 'uses' => 'MessagingController@create'));
Route::post('dashboard/messaging/create_post', array('before' => 'auth', 'uses' => 'MessagingController@createPost'));

// Register
Route::get('/', array('uses' => 'RegistrationController@create'));
Route::post('register/create_post', array('uses' => 'RegistrationController@createPost'));
Route::get('register/waiver/{id}', array('uses' => 'RegistrationController@waiver'));
Route::post('register/waiver/create_post', array('uses' => 'RegistrationController@waiverPost'));
Route::get('register/payments/{id}', array('uses' => 'RegistrationController@payments'));
Route::post('register/payments/create_post', array('uses' => 'RegistrationController@paymentsPost'));
Route::get('register/thanks', array('uses' => 'RegistrationController@thanks'));
